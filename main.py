import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
from fastapi import FastAPI, HTTPException
import tensorflow as tf
from tensorflow import keras
import numpy as np
from pydantic import BaseModel

app = FastAPI()

sentiment_model = keras.models.load_model('trax_sentiment')


class predictionText(BaseModel):
    text: str

class sentimentPrediction(predictionText):
    prediction: str


@app.post("/predict", response_model = sentimentPrediction, status_code=200)
def get_prediction(payload: predictionText):
    
    text = payload.text

    if np.argmax(sentiment_model.predict(x=[text])) == 1:
        sentiment = 'Positive'  
    else:
        sentiment = 'Negative'

    if not sentiment_model:
        raise HTTPException(status_code=400, detail="Model not found.")

    response_object = {"text": text, "prediction": sentiment}
    return response_object
    